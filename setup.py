#!/usr/bin/env python

from setuptools import setup

setup(
    # GETTING-STARTED: set your app name:
    name='Innovacion',
    # GETTING-STARTED: set your app version:
    version='1.0',
    # GETTING-STARTED: set your app description:
    description='The backend application for Innovacion',
    # GETTING-STARTED: set author name (your name):
    author='Shinjan Mitra, Shashwata Mandal',
    # GETTING-STARTED: set author email (your email):
    author_email='ieminnovacion@gmail.com',
    # GETTING-STARTED: set author url (your url):
    url='http://www.python.org/sigs/distutils-sig/',
    # GETTING-STARTED: define required django version:
    install_requires=[
        'Django==1.8.4',
        'MySQL-python',
        'djangorestframework'
    ],
    dependency_links=[
        'https://pypi.python.org/simple/django/'
    ],
)
