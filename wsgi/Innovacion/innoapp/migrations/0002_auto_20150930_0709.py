# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('innoapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Managers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
            ],
        ),
        migrations.DeleteModel(
            name='User',
        ),
        migrations.RenameField(
            model_name='event',
            old_name='event_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='participant',
            old_name='participant',
            new_name='contact',
        ),
        migrations.RenameField(
            model_name='team',
            old_name='team_name',
            new_name='name',
        ),
        migrations.AddField(
            model_name='event',
            name='contact_detail',
            field=models.CharField(default=None, max_length=40),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='genre',
            field=models.CharField(default=None, max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='pdf_url',
            field=models.FileField(default=None, max_length=200, upload_to=b''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='prizes',
            field=models.CharField(default=None, max_length=40),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='register_url',
            field=models.URLField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='event',
            name='rules',
            field=models.TextField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eventregistraion',
            name='event_name',
            field=models.OneToOneField(default=None, to='innoapp.Event'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='dept',
            field=models.CharField(default=None, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='inst',
            field=models.CharField(default=None, max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='name',
            field=models.CharField(default=None, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='part_id',
            field=models.CharField(default=None, max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='user',
            field=models.OneToOneField(default=None, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='participant',
            name='year',
            field=models.PositiveSmallIntegerField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='registration',
            name='time_stamp',
            field=models.DateField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='team',
            name='event',
            field=models.ForeignKey(default=None, to='innoapp.Event'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='team',
            name='participants',
            field=models.ManyToManyField(to='innoapp.Participant'),
        ),
        migrations.AlterField(
            model_name='eventregistraion',
            name='participant',
            field=models.OneToOneField(to='innoapp.Participant'),
        ),
        migrations.AddField(
            model_name='managers',
            name='event',
            field=models.ForeignKey(to='innoapp.Event'),
        ),
    ]
