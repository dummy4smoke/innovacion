from django.contrib import admin

from innoapp.models import *

class EventAdmin(admin.ModelAdmin):
    pass

class ParticipantAdmin(admin.ModelAdmin):
    pass

admin.site.register(Event, EventAdmin)
admin.site.register(Participant, ParticipantAdmin)
